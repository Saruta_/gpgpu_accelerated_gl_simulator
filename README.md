# PEGASE: GPGPU circuit simulator

This project aims to get a fully fonctional hardware (gate level) simulation
running on gpus. Most of the idea comes from the following paper: [1].

Basically, all the emulation is done on truth tables. All this simulation is
fully oblivious at the moment, meaning that it respects temporality.

On that simulation, we add clustering. Each cluster is a sub-circuit independant
of the remaining part of it. Thus, we can run multiple work-group (cluster) at
the same moment.

## Usage

![](demo.gif)

## References

[1] Debapriya Chatterjee , Andrew Deorio , Valeria Bertacco, Gate-Level Simulation with GPU Computing
