module adder64(a, b, s);
	input [63:0] a, b;
	output [63:0] s;

	assign s = a + b;
endmodule

