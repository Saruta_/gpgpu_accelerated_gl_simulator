module adder(a, b, cin, s, cout);
	input a, b, cin;
	output s, cout;

	wire p, g;
	assign p = a ^ b;
	assign g = a & b;
	assign s = p ^ cin;
	assign cout = g | (p & cin);

endmodule

module adder4(a, b, s);
	input [3:0] a, b;
	output [3:0] s;
	wire [3:0] c;

	adder inst1(.a(a[0]), .b(b[0]), .cin(1'b0), .s(s[0]), .cout(c[0]));
	adder inst2(.a(a[1]), .b(b[1]), .cin(c[0]), .s(s[1]), .cout(c[1]));
	adder inst3(.a(a[2]), .b(b[2]), .cin(c[1]), .s(s[2]), .cout(c[2]));
	adder inst4(.a(a[3]), .b(b[3]), .cin(c[2]), .s(s[3]), .cout(c[3]));
endmodule
