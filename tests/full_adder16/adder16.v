module adder(a, b, cin, s, cout);
	input a, b, cin;
	output s, cout;

	wire p, g;
	assign p = a ^ b;
	assign g = a & b;
	assign s = p ^ cin;
	assign cout = g | (p & cin);

endmodule

module adder8(a, b, cin, s, cout);
	input [7:0] a, b;
	input cin;
	output [7:0] s;
	output cout;
	wire [6:0] c;

	adder inst1(.a(a[0]), .b(b[0]), .cin(cin), .s(s[0]), .cout(c[0]));
	adder inst2(.a(a[1]), .b(b[1]), .cin(c[0]), .s(s[1]), .cout(c[1]));
	adder inst3(.a(a[2]), .b(b[2]), .cin(c[1]), .s(s[2]), .cout(c[2]));
	adder inst4(.a(a[3]), .b(b[3]), .cin(c[2]), .s(s[3]), .cout(c[3]));
	adder inst5(.a(a[4]), .b(b[4]), .cin(c[3]), .s(s[4]), .cout(c[4]));
	adder inst6(.a(a[5]), .b(b[5]), .cin(c[4]), .s(s[5]), .cout(c[5]));
	adder inst7(.a(a[6]), .b(b[6]), .cin(c[5]), .s(s[6]), .cout(c[6]));
	adder inst8(.a(a[7]), .b(b[7]), .cin(c[6]), .s(s[7]), .cout(cout));
endmodule

module adder16(a, b, s);
	input [15:0] a, b;
	output [15:0] s;
	wire [2:0] c;

	adder8 inst1(.a(a[7:0]), .b(b[7:0]), .cin(1'b0), .s(s[7:0]), .cout(c[0]));
	adder8 inst2(.a(a[15:8]), .b(b[15:8]), .cin(c[0]), .s(s[15:8]), .cout(c[1]));
endmodule
