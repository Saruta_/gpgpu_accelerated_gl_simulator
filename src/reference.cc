#include "reference.hh"

int reference(Netlist& netlist)
{
	// Some simple gates
	char g_nand[4] = {1, 1, 1, 0};
	char g_nor[4] = {1, 0, 0, 0};
	char g_and[4] = {0, 0, 0, 1};
	char g_or[4] = {0, 1, 1, 1};
	char g_xor[4] = {0, 1, 1, 0};
	char g_not[2] = {1, 0};
	char *gates_truth[] = {g_and, g_or, g_xor, g_nand, g_nor, g_not};
	for (auto& c : netlist.clusters)
	{
		unsigned gate_addr = 0;

		for (int i = 0; i < c.N; i++)
		{
			unsigned short w = c.widths[i];
			for (int j = 0; j < w; j++)
			{
				gate_t gate = c.gates[gate_addr + j];
				pin_t pin1 = c.input_ids[gate.inputs + 0];
				int i1 = pin1.type
					? Netlist::get_idx(c.inputs.data(), pin1.val)
					: Netlist::get_idx(c.wires.data(), pin1.val);
				pin_t pin2;
				int i2;
				if (gate.type != NOT_GATE) {
					pin2 = c.input_ids[gate.inputs + 1];
					i2 = pin2.type
						? Netlist::get_idx(c.inputs.data(), pin2.val)
						: Netlist::get_idx(c.wires.data(), pin2.val);
				}

				char *gate_truth = gates_truth[gate.type];

				pin_t pino = c.output_ids[gate.outputs + 0];
				if (gate.type != NOT_GATE)
					Netlist::set_idx(pino.type
							 ? c.outputs.data()
							 : c.wires.data(),
							 pino.val, gate_truth[(i1 << 1) + i2]);
				else
					Netlist::set_idx(pino.type
							 ? c.outputs.data()
							 : c.wires.data(),
							 pino.val, gate_truth[i1]);
			}
			gate_addr += w;
		}
	}
	return 0;
}
