#include "cluster.hh"

#include <iostream>

void Cluster::dump_info()
{
	std::cout << "\tCluster max width: " <<  width << std::endl;
	std::cout << "\tCluster max height: " <<  N << std::endl;
	std::cout << "\tCluster input length (bytes): " << inputs.size() << std::endl;
	std::cout << "\tCluster wires length (bytes): " << wires.size() << std::endl;
	std::cout << "\tCluster output length (bytes): " << outputs.size() << std::endl;
	std::cout << "\tCluster gate count: " << gates.size() << std::endl;
}
