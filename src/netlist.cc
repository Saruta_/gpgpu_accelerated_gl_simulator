#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <iostream>
#include <string>
#include <stdexcept>
#include <vector>

#include "netlist.hh"

namespace pt = boost::property_tree;

int Netlist::get_idx(char *v, int idx)
{
	return (v[idx / 8] >> (idx % 8)) & 0x1;
}

void Netlist::set_idx(char *v, int idx, char val)
{
	v[idx / 8] = val
		? v[idx / 8] | (1 << (idx % 8))
		: v[idx / 8] & ~(1 << (idx % 8));
}

pin_t Netlist::parse_pin(const pt::ptree& gate_root,
			 std::map<size_t, size_t>& wire_map,
			 std::map<size_t, size_t>& input_map,
			 std::map<size_t, size_t>& output_map,
			 size_t& wire_cnt, size_t& input_cnt, size_t& output_cnt)
{
	pin_t rv;
	rv.val = static_cast<size_t>(gate_root.get<size_t>("val"));
	std::string type = gate_root.get<std::string>("signal_type");
	if (type == "INPUT")
	{
		rv.type = INPUT;
		if (input_map.find(rv.val) != input_map.end())
			rv.val = input_map[rv.val];
		else
		{
			input_map[rv.val] = input_cnt;
			rv.val = input_map[rv.val];
			input_cnt++;
		}
	}
	else if (type == "OUTPUT")
	{
		rv.type = OUTPUT;
		if (output_map.find(rv.val) != output_map.end())
			rv.val = output_map[rv.val];
		else
		{
			output_map[rv.val] = output_cnt;
			rv.val = output_map[rv.val];
			output_cnt++;
		}
	}
	else //(type == "WIRE")
	{
		rv.type = WIRE;
		/* this value does not exist yet */
		if (wire_map.find(rv.val) != wire_map.end())
			rv.val = wire_map[rv.val];
		else
		{
			wire_map[rv.val] = wire_cnt;
			rv.val = wire_map[rv.val];
			wire_cnt++;
		}
	}
	return rv;
}

gate_type_t Netlist::strtype_to_gatetype(std::string strtype)
{
	if (strtype == "and")
		return AND_GATE;
	else if (strtype == "or")
		return OR_GATE;
	else if (strtype == "xor")
		return XOR_GATE;
	else if (strtype == "nand")
		return NAND_GATE;
	else if (strtype == "nor")
		return NOR_GATE;
	else if (strtype == "not")
		return NOT_GATE;
	else
		abort();
}

static std::string pin_type[] = {
	[WIRE] = "wire",
	[INPUT] = "input",
	[OUTPUT] = "output",
};
static std::string gate_type[] = {
	[AND_GATE] = "and",
	[OR_GATE] = "or",
	[XOR_GATE] = "xor",
	[NAND_GATE] = "nand",
	[NOR_GATE] = "nor",
	[NOT_GATE] = "not",
};

void Netlist::parse_json(int argc, char *argv[])
{
	if (argc != 2)
		throw std::invalid_argument("Invalid syntax");

	pt::ptree root;

	try {
		pt::read_json(argv[1], root);
	} catch (const pt::json_parser_error& err) {
		std::cerr << "ERROR json read exception" << std::endl;
		std::cerr << "line " << err.line() << ": "
			<< err.message() << std::endl;
	}

	std::vector<char> values_vect;

	size_t input_cnt = root.get_child("inputs").size();
	size_t output_cnt = root.get_child("outputs").size();
	/* XXX check size */
	for (pt::ptree::value_type &values : root.get_child("values"))
		values_vect.push_back(values.second.get_value<unsigned>());

	/* XXX move to by-cluster input */
	/* parse clusters */
	for (pt::ptree::value_type &cluster : root.get_child("gates"))
	{
		Cluster current_cluster;
		std::map<size_t, size_t>& input_map = current_cluster.input_map;
		std::map<size_t, size_t>& output_map = current_cluster.output_map;
		std::map<size_t, size_t> wire_map;
		size_t wire_cnt = 2;
		int gate_idx = 0; size_t level_idx = 0;
		current_cluster.width = 0;
		wire_map[0] = 0;
		wire_map[1] = 1;
		size_t cluster_input_cnt = 0;
		size_t cluster_output_cnt = 0;

		/* parse levels */
		for (pt::ptree::value_type &level : cluster.second)
		{
			int width = 0;
			/* parse actual gates */
			for (pt::ptree::value_type &gate : level.second)
			{
				gate_t gate_desc;
				gate_desc.type = strtype_to_gatetype(gate.second.get<std::string>("type"));
			        current_cluster.input_ids.push_back(parse_pin(gate.second.get_child("pin1"),
									      wire_map, input_map, output_map,
									      wire_cnt, cluster_input_cnt, cluster_output_cnt));
				if (gate_desc.type != NOT_GATE) {
					current_cluster.input_ids.push_back(parse_pin(gate.second.get_child("pin2"),
										      wire_map, input_map, output_map,
										      wire_cnt, cluster_input_cnt, cluster_output_cnt));
					gate_desc.inputs = current_cluster.input_ids.size() - 2;
				} else {
					gate_desc.inputs = current_cluster.input_ids.size() - 1;
				}

				current_cluster.output_ids.push_back(parse_pin(gate.second.get_child("pino"),
									      wire_map, input_map, output_map,
									      wire_cnt, cluster_input_cnt, cluster_output_cnt));
				gate_desc.outputs = current_cluster.output_ids.size() - 1;
				current_cluster.gates.push_back(gate_desc);
				width++;
			}
			level_idx++;
			level_t level_st = {.width = width};
			gate_idx += width;
			current_cluster.levels.push_back(level_st);
			current_cluster.widths.push_back(width);
			if (width > current_cluster.width)
				current_cluster.width = width;
		}
		current_cluster.input_cnt = cluster_input_cnt;
		current_cluster.output_cnt = cluster_output_cnt;
		current_cluster.N = level_idx;
		current_cluster.inputs.resize((cluster_input_cnt + 7) / 8);
		current_cluster.outputs.resize((cluster_output_cnt + 7) / 8);
		current_cluster.wires.resize((wire_cnt + 7) / 8);
		this->clusters.push_back(current_cluster);
	}
	this->inputs.resize((input_cnt + 7) / 8);
	this->outputs.resize((output_cnt + 7) / 8);

#ifdef DEBUG
	int i = 0;
	for (const auto& c: this->clusters)
	{
		std::cout << "cluster " << i << std::endl;
		for (const auto& g: c.gates)
			std::cout << "\t\t" << gate_type[g.type] << ": <" << g.inputs<< ", " << g.outputs << ">" << std::endl;
		std::cout << "\tinput_ids" << std::endl;
		for (const auto& item: c.input_ids)
			std::cout << "\t\t" << pin_type[item.type] << ":" << item.val << std::endl;
		std::cout << "\toutput_ids" << std::endl;
		for (const auto& item: c.output_ids)
			std::cout << "\t\t" << pin_type[item.type] << ":" << item.val << std::endl;
		i++;
	}
#endif

	for (unsigned i = 0; i < values_vect.size(); i++)
		set_idx(this->inputs.data(), i, values_vect[i] != 0);

	for (auto& c: this->clusters)
	{
		for (size_t i = 0; i < c.input_cnt; i++)
		{
			auto it = std::find_if(c.input_map.begin(), c.input_map.end(),
					       [&i](const std::pair<size_t, size_t> &p)
					       {
						       return p.second == i;
					       });
			size_t original_i = it->first;
			set_idx(c.inputs.data(), i, values_vect[original_i] != 0);
		}
	}
}

void Netlist::merge_output()
{
	for (auto& c: this->clusters)
	{
		for (size_t i = 0; i < c.output_cnt; i++)
		{
			size_t val = get_idx(c.outputs.data(), i);
			auto it = std::find_if(c.output_map.begin(), c.output_map.end(),
					       [&i](const std::pair<size_t, size_t> &p)
					       {
						       return p.second == i;
					       });
			size_t original_i = it->first;			
			set_idx(outputs.data(), original_i, val);
		}
	}
}

void Netlist::dump_info()
{
	unsigned i = 0;
	std::cout << "Netlist clusters info:" << std::endl;
	for (auto c : clusters)
	{
		std::cout << "Cluster " << i << " info:" << std::endl;
		c.dump_info();
		i++;
	}
	std::cout << "Netlist global info:" << std::endl;
	std::cout << "\tNetlist input length (bytes): " << inputs.size() << std::endl;
        std::cout << "\tNetlist output length (bytes): " << outputs.size() << std::endl;
}
