#pragma once

#include <vector>
#include <map>

typedef enum {
	WIRE = 0,
	INPUT = 1,
	OUTPUT = 2,
} signal_type_t;

typedef enum {
	AND_GATE = 0,
	OR_GATE  = 1,
	XOR_GATE,
	NAND_GATE,
	NOR_GATE,
	NOT_GATE
} gate_type_t;

typedef enum {
	LOW = 0,
	HIGH = 1
} signal_t;

typedef struct {
	int width;
} __attribute__((packed)) level_t;

typedef struct {
	signal_type_t type;
	unsigned val;
} __attribute__((packed)) pin_t; 

typedef struct {
	gate_type_t type;
	unsigned inputs;
	unsigned outputs;
} __attribute__((packed)) gate_t;

struct Cluster
{
	void dump_info();

	std::vector<gate_t> gates;
	std::vector<level_t> levels;
	std::vector<pin_t> input_ids;
	std::vector<pin_t> output_ids;
	std::vector<unsigned short> widths;
	std::vector<char> inputs;
	std::vector<char> outputs;
	std::vector<char> wires;
	std::map<size_t, size_t> wire_map;
	std::map<size_t, size_t> input_map;
	std::map<size_t, size_t> output_map;
	size_t input_cnt;
	size_t output_cnt;
	int width;
	int N;
};
