#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <bitset>
#include <iostream>
#include <sstream>
#include <vector>

#include "util.hpp"
#include "err_code.h"

#include "netlist.hh"
#include "reference.hh"

#define N_TEST 1000

int main(int argc, char *argv[])
{
	std::string simu_path = argv[1];
	argc--; argv++;
	Netlist netlist;
	netlist.parse_json(argc, argv);
	std::vector<cl::Buffer> d_inputs;
	std::vector<cl::Buffer> d_input_ids;
	std::vector<cl::Buffer> d_outputs;
	std::vector<cl::Buffer> d_output_ids;
	std::vector<cl::LocalSpaceArg> d_wires;
	std::vector<cl::Buffer> d_gates;
	std::vector<cl::Buffer> d_levels;

	netlist.dump_info();

	try
	{
		// Initialize kernel & command queue
		cl::Context context(CL_DEVICE_TYPE_DEFAULT);
		cl::Program program(context,
				    util::loadProgram(simu_path + "/simulator.cl"),
				    true);
		cl::CommandQueue queue(context, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE);
		cl::make_kernel<cl::Buffer, cl::Buffer, cl::Buffer,
				cl::Buffer, cl::LocalSpaceArg,
				cl::Buffer, cl::Buffer, int>
			gl_local_truth(program, "simulate");

		// Manage output buffer
		for (auto c : netlist.clusters)
		{
			d_inputs.push_back(cl::Buffer(context, std::begin(c.inputs),
						      std::end(c.inputs), true));
			d_outputs.push_back(cl::Buffer(context, CL_MEM_WRITE_ONLY,
						       sizeof(char) * c.outputs.size()));
			d_wires.push_back(cl::Local(sizeof(char) * c.wires.size()));
			d_gates.push_back(cl::Buffer(context,
					     CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
					     sizeof(gate_t) * c.gates.size(),
						     c.gates.data()));
			d_input_ids.push_back(cl::Buffer(context,
						 CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
						 sizeof(pin_t) * c.input_ids.size(),
							 c.input_ids.data()));
			d_output_ids.push_back(cl::Buffer(context,
						  CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
						  sizeof(pin_t) * c.output_ids.size(),
							  c.output_ids.data()));
			d_levels.push_back(cl::Buffer(context, std::begin(c.widths),
						      std::end(c.widths), true));
		        if ((c.width + 1) >= 1024)
			{
				std::istringstream is("Cluster too big to fit into a work group (must be < 1024).");
				throw std::logic_error(is.str());
			}
		}

		util::Timer timer1;

		// Execute kernel
		for (int i = 0; i < N_TEST; i++)
		{
			for (size_t i = 0; i < netlist.clusters.size(); ++i) {
				auto& c = netlist.clusters[i];
				gl_local_truth(cl::EnqueueArgs(queue, cl::NDRange(c.width + 1)),
					       d_levels[i], d_gates[i], d_inputs[i],
					       d_outputs[i], d_wires[i],
					       d_input_ids[i], d_output_ids[i], c.N);
			}
			queue.enqueueBarrierWithWaitList();
		}
		queue.finish();

		// Copy data back to host
		for (size_t i = 0; i < netlist.clusters.size(); ++i)
		{
			auto& c = netlist.clusters[i];
			cl::copy(queue, d_outputs[i],
				 c.outputs.begin(), c.outputs.end());
		}

		uint64_t t1 = timer1.getTimeMicroseconds();
		netlist.merge_output();
		// Display it
		std::cout << "Kernel ended !" << std::endl;
		std::cout << "On GPU: " << std::endl;
		for (const auto& elt : netlist.inputs)
			std::cout << "\tinputs: " << std::bitset<8>(elt) << std::endl;
		for (const auto& elt : netlist.outputs)
			std::cout << "\telt: " << std::bitset<8>(elt) << std::endl;

		std::cout << "\tt1 (gpu): " << t1 << "us" << std::endl;

		util::Timer timer2;

		for (int i = 0; i < N_TEST; i++)
			reference(netlist);

		uint64_t t2 = timer2.getTimeMicroseconds();

		// Display it
		std::cout << "CPU thread ended !" << std::endl;
		netlist.merge_output();
		for (const auto& elt : netlist.inputs)
			std::cout << "\tinputs: " << std::bitset<8>(elt) << std::endl;
		for (const auto& elt : netlist.outputs)
			std::cout << "\telt: " << std::bitset<8>(elt) << std::endl;

		std::cout << "\tt2 (cpu): " << t2 << "us" << std::endl;
	}
	catch (cl::Error& err)
	{
		std::cout << "Exception\n";
		std::cerr << "Error: " << err.what()
			<< "(" << err_code(err.err()) << ")" << std::endl;
	}
}
