#pragma once

# include <vector>
# include <map>
# include <boost/property_tree/ptree.hpp>
# include <boost/property_tree/json_parser.hpp>
# include "cluster.hh"

struct Netlist
{
	void parse_json(int argc, char *argv[]);
	void dump_info();

	/* helpers */
	static int get_idx(char *v, int idx);
	static void set_idx(char *v, int idx, char val);

	static pin_t parse_pin(const boost::property_tree::ptree& gate_root,
			       std::map<size_t, size_t>& wire_map,
			       std::map<size_t, size_t>& input_map,
			       std::map<size_t, size_t>& output_map,			       
			       size_t& wire_cnt, size_t& input_cnt, size_t& output_cnt);
	static gate_type_t strtype_to_gatetype(std::string strtype);
	void merge_output();

	std::vector<char> inputs;
	std::vector<char> wires;
	std::vector<char> outputs;
	std::vector<pin_t> input_ids;
	std::vector<pin_t> output_ids;
	std::vector<gate_t> gates;
	std::vector<level_t> levels;
	std::vector<unsigned short> widths;
	int width;
	int N;
	std::vector<Cluster> clusters;
};
