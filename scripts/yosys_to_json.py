#!/bin/env python3
import argparse
import graphviz
import json
import os
import pprint
import sys

"""
Data structures:
* yosys_bits: dictionary that maps yosys bits to pegase ones
    key : yosys bits
    data :
    {
        'signal_type': 'INPUT'|'OUTPUT'|'WIRE',
        'val' : allocated value in corresponding space (input/output/wire)}
    }
* pegase_gates: vector of generated pegase gates
    data :
    {
        'id' : 'unique identifier'
        'type': 'or'|'xor'| ... in PEGASE_KNOWN_GATES,
        'pin1': see yosys bits data above
        'pin2': see yosys bits data above
        'pino': see yosys bits data above
    }
"""

PEGASE_KNOWN_GATES = ["xor", "nand", "nor", "or", "and"]
TECHMAP_KNOWN_GATES = ["_XOR_", "_NAND_", "_NOR_", "_OR_", "_AND_", "_NOT_"]

class YosysJsonParser:
    PEGASE_WIRE='WIRE'
    PEGASE_INPUT='INPUT'
    PEGASE_OUTPUT='OUTPUT'

    def __init__(self, args):
        yosys_file = args.output
        self.modulename = args.module
        self.basename = os.path.splitext(os.path.basename(yosys_file))[0]
        with open(yosys_file, 'r') as f:
            data = json.load(f)
            self.module = data['modules'][self.modulename]
        # used to allocate pins
        self.yosys_bits = {}
        self.pegase = {}
        self.pegase_gates = []
        self.pegase_input_cnt = 0
        self.pegase_output_cnt = 0
        self.pegase_wire_cnt = 2
        self.pegase_clusters = {}
        self.levelized_clusters = []
        self.balanced_levelized_clusters = []
        self.verbose = args.verbose

    def insert_port_bit(self, port, port_type):
        value = 0
        if port_type == self.PEGASE_INPUT:
            value = self.pegase_input_cnt
            self.pegase_input_cnt += 1
        else:
            value = self.pegase_output_cnt
            self.pegase_output_cnt += 1

        bit = { "signal_type": port_type, "val": value }
        self.yosys_bits[port] = bit

    def insert_port_bits(self, port, port_type):
        for b in port['bits']:
            self.insert_port_bit(b, port_type)

    def insert_bit(self, bits):
        for b in bits:
            if not b in self.yosys_bits:
                value = self.pegase_wire_cnt
                self.pegase_wire_cnt += 1
                new_bit = {"signal_type": self.PEGASE_WIRE, "val": value}
                self.yosys_bits[b] = new_bit

    def append_port_to(self, vect, yosys_ports, port):
        for i, b in enumerate(yosys_ports[port]['bits']):
            ident = '%s_%i' % (port, i)
            vect.append(ident)

    def generate_ports(self):
        yosys_ports = self.module['ports']
        pegase_inputs = []
        pegase_outputs = []

        for p in yosys_ports:
            if yosys_ports[p]['direction'] == 'input':
                self.append_port_to(pegase_inputs, yosys_ports, p)
                self.insert_port_bits(yosys_ports[p], self.PEGASE_INPUT)
            else:
                self.append_port_to(pegase_outputs, yosys_ports, p)
                self.insert_port_bits(yosys_ports[p], self.PEGASE_OUTPUT)

        self.pegase['inputs'] = pegase_inputs
        self.pegase['outputs'] = pegase_outputs

    def generate_wires(self):
        yosys_nets = self.module['netnames']

        for net in yosys_nets:
            bits = yosys_nets[net]['bits']
            self.insert_bit(bits)

    def generate_cells(self):
        yosys_cells = self.module['cells']
        for c in yosys_cells:
            cell = yosys_cells[c]
            cell_type = cell['type'][1:]
            assert cell_type in PEGASE_KNOWN_GATES or cell_type in TECHMAP_KNOWN_GATES, \
                    "gate %r not known, please add it to the project" % cell_type
            # XXX does inputs are always A, B, C ... in yosys?
            # XXX does outputs are always Y ... in yosys?
            # XXX simply arbitrary number pin handling
            if cell_type in TECHMAP_KNOWN_GATES:
                cell_type = cell_type[1:-1].lower()
            cell_pin1_nr = cell['connections']['A'][0]
            pin1 = self.yosys_bits[cell_pin1_nr]
            pin2 = 0
            if cell_type != 'not':
                cell_pin2_nr = cell['connections']['B'][0]
                pin2 = self.yosys_bits[cell_pin2_nr]
            cell_pino_nr = cell['connections']['Y'][0]
            pino = self.yosys_bits[cell_pino_nr]
            c_ = c.replace(':', '_')
            if cell_type == 'not':
                gate = {'id': c_, 'type': cell_type, 'pin1' : pin1,
                        'pin2' : None, 'pino': pino}
                self.pegase_gates.append(gate)
            else:
                gate = {'id': c_, 'type': cell_type, 'pin1' : pin1,
                        'pin2' : pin2, 'pino': pino}
                self.pegase_gates.append(gate)

    def net_hash(self, net):
        return tuple(sorted(net.items()))

    def get_incoming_edges(self):
        yosys_ports = self.module['ports']
        edge_from = {}
        for p in yosys_ports:
            if yosys_ports[p]['direction'] == 'input':
                bits = yosys_ports[p]['bits']
                for i, b in enumerate(bits):
                    identifier = '%s_%d' % (p, i)
                    edge_from[self.net_hash(self.yosys_bits[b])] = \
                        {'id': identifier, \
                                'type': 'PI', 'pin1' : None,  \
                                'pin2' : None, 'pino' : \
                                    self.yosys_bits[b]}

        for n in self.pegase_gates:
            edge_from[self.net_hash(n['pino'])] = n

        return edge_from

    def get_outcoming_edges(self):
        yosys_ports = self.module['ports']
        edge_to = {}
        for p in yosys_ports:
            if yosys_ports[p]['direction'] == 'output':
                bits = yosys_ports[p]['bits']
                for i, b in enumerate(bits):
                    identifier = '%s_%d' % (p, i)
                    po =  {'id': identifier, \
                                'type': 'PO', 'pin1' : self.yosys_bits[b],  \
                                'pin2' : None, 'pino' : None}
                    if self.net_hash(self.yosys_bits[b]) in edge_to:
                        edge_to[self.net_hash(self.yosys_bits[b])] += [po]
                    else:
                        edge_to[self.net_hash(self.yosys_bits[b])] = [po]

        for n in self.pegase_gates:
            if self.net_hash(n['pin1']) in edge_to:
                edge_to[self.net_hash(n['pin1'])] += [n]
            else:
                edge_to[self.net_hash(n['pin1'])] = [n]
            if n['pin2']:
                if self.net_hash(n['pin2']) in edge_to:
                    edge_to[self.net_hash(n['pin2'])] += [n]
                else:
                    edge_to[self.net_hash(n['pin2'])] = [n]

        return edge_to

    def topological_sort_1(self, gate, gates_sorted, gates_tagged, edges_to):
        next_gates = [] if gate['pino'] is None else edges_to[self.net_hash(gate['pino'])]
        assert gate is not None, "levelize_1: gate None!"

        for g in next_gates:
            if g is not None and g['id'] not in gates_tagged:
                self.topological_sort_1(g, gates_sorted, gates_tagged, edges_to)

        gates_tagged[gate['id']] = True;
        gates_sorted.append(gate) 
        assert gate is not None, "levelize_1: gate None!"


    def topological_sort(self):
        """
        The level of a gate is the longest path from the primary inputs to it.
        A circuit is basically just a DAG (direct acyclic graph), so to compute
        this longest paths which is ordinary a NP complete problem, we could
        also compute its topological sort and compute all the levels lineary.
        """
        pegase_gates_tagged = {}
        pegase_gates_sorted = []
        edge_to = self.get_outcoming_edges()
        # topological sort
        for gate in self.pegase_gates:
            if gate['id'] not in pegase_gates_tagged:
               self.topological_sort_1(gate, pegase_gates_sorted, pegase_gates_tagged, edge_to)

        return pegase_gates_sorted

    def levelize1(self):
        sorted_gates = self.topological_sort()
        edge_from = self.get_incoming_edges()

        gate_levels = {}
        max_level = 0

        for g in reversed(sorted_gates):
            pin1 = None; pin2 = None
            if g['pin1'] and g['pin1']['signal_type'] != 'INPUT':
                pin1 = edge_from[self.net_hash(g['pin1'])]
            if g['pin2'] and g['pin2']['signal_type'] != 'INPUT':
                pin2 = edge_from[self.net_hash(g['pin2'])]
            pin1_nr = 0; pin2_nr = 0
            if pin1:
                pin1_nr = 0 if pin1['type'] == 'PI' else gate_levels[pin1['id']]
            if pin2:
                pin2_nr = 0 if pin2['type'] == 'PI' else gate_levels[pin2['id']]
            pin_nr = max(pin1_nr, pin2_nr)
            gate_levels[g['id']] = pin_nr + 1
            if (pin_nr + 1) > max_level:
                max_level = pin_nr + 1

        return gate_levels, max_level

    def levelize(self):
        gate_levels, max_level = self.levelize1()
        levelized_gates = [[] for i in range(max_level - 1)]

        for g in self.pegase_gates:
            level = gate_levels[g['id']]
            levelized_gates[level - 1].append(g)

        return levelized_gates

    def levelize_cluster(self, cluster, gate_levels, max_level):
        levelized_gates = [[] for i in range(max_level - 1)]
        for g in cluster:
            if g['type'] != 'PI':
                level = gate_levels[g['id']]
                levelized_gates[level - 1].append(g)

        return levelized_gates

    def levelize_clusters(self):
        gate_levels, max_level = self.levelize1()
        levelized_clusters = []
        for key in self.pegase_clusters:
            cluster = self.pegase_clusters[key]
            levelized_clusters.append(self.levelize_cluster(cluster,
                                                            gate_levels,
                                                            max_level))
        # squash clusters
        levelized_squashed_clusters = []
        for cluster in levelized_clusters:
            levelized_squashed_clusters.append(
                list(filter(lambda elt: elt != [], cluster)))
        return levelized_squashed_clusters

    def clusterize1(self, edge_from, gate, key):
        if gate['pin1'] is not None:
            gate1 = edge_from[self.net_hash(gate['pin1'])]
            if not gate1 in self.pegase_clusters[key]:
                self.pegase_clusters[key].append(gate1)
            self.clusterize1(edge_from, gate1, key)
        if gate['pin2'] is not None:
            gate2 = edge_from[self.net_hash(gate['pin2'])]
            if not gate2 in self.pegase_clusters[key]:
                self.pegase_clusters[key].append(gate2)
            self.clusterize1(edge_from, gate2, key)
    
    def clusterize(self):
        yosys_ports = self.module['ports']
        edge_from = self.get_incoming_edges()
        for p in yosys_ports:
            # iterate on output
            if yosys_ports[p]['direction'] == 'output':
                bits = yosys_ports[p]['bits']
                for i, b in enumerate(bits):
                    net = self.yosys_bits[b]
                    gate = edge_from[self.net_hash(net)]
                    self.pegase_clusters[self.net_hash(net)] = [gate]
                    self.clusterize1(edge_from, gate, self.net_hash(net))

    def compute_gate_level(self, cluster, gate):
        for i, l in enumerate(cluster):
            if gate in l:
                return i
        return -1

    def compute_minimum_gate_level(self, cluster, gates):
        l = -1
        for g in gates:
            if g['type'] != 'PO':
                l_candidate = self.compute_gate_level(cluster, g)
                if l_candidate == -1:
                    continue

                if l == -1:
                    l = l_candidate
                else:
                    if l_candidate < l:
                        l = l_candidate
        return l

    def balance_cluster(self, cluster):
        # reserve new cluster
        balanced_cluster = [[] for i in range(len(cluster))]
        edge_to = self.get_outcoming_edges()
        # iterate through each level
        for l in cluster:
            for g in l:
                next_gates = edge_to[self.net_hash(g['pino'])]
                l = self.compute_minimum_gate_level(cluster, next_gates)
                if l == -1:
                    balanced_cluster[len(cluster) - 1].append(g)
                else:
                    balanced_cluster[l - 1].append(g)

        return balanced_cluster
                    
    def balance_clusters(self, levelized_gates):
        new_balanced_clusters = []
        for c in levelized_gates:
            new_balanced_clusters.append(self.balance_cluster(c))

        return new_balanced_clusters
    def compute_cluster_intersection(self, cluster1, cluster2):
        res = []
        for g1 in cluster1:
            for g2 in cluster2:
                if g1 == g2 and g1['type'] != 'PI' and g2['type'] != 'PI':
                    res.append(g2)
        return res

    def compute_cluster_overlap(self, cluster1, cluster2):
        overlaping_part = self.compute_cluster_intersection(cluster1, cluster2)
        filtered_cluster1 = list(filter((lambda elt: elt['type'] != 'PI'), cluster1))
        cluster1_len = len(filtered_cluster1)
        filtered_cluster2 = list(filter((lambda elt: elt['type'] != 'PI'), cluster2))
        cluster2_len = len(filtered_cluster2)
        cluster1_overlap = len(overlaping_part) * 100 / cluster1_len
        cluster2_overlap = len(overlaping_part) * 100 / cluster2_len
        #print('CLUSTER 1:')
        #print('\tlength %d' % cluster1_len)
        #print('\toverlap: %d%%' % cluster1_overlap)
        #print('CLUSTER 2:')
        #print('\tlength %d' % cluster2_len)
        #print('\toverlap: %d%%' % cluster2_overlap)
        #print('total overlap: %d' % len(overlaping_part))
        return cluster1_overlap

    def compute_clusters_overlap(self):
        for k1 in self.pegase_clusters:
            c1 = self.pegase_clusters[k1]
            for k2 in self.pegase_clusters:
                c2 = self.pegase_clusters[k2]
                if c1 != c2:
                    self.compute_cluster_overlap(c1, c2)

    def fetch_closest_clusters(self, clusters):
        # the function returns this tuple if the closest clusters are of 0%
        res = (None, None, 0)
        for k1 in clusters:
            c1 = clusters[k1]
            for k2 in clusters:
                c2 = clusters[k2]
                if k1 != k2:
                    overlap = self.compute_cluster_overlap(c1, c2)
                    if res[2] < overlap:
                        res = (k1, k2, overlap)
        return res

    def merge_clusters(self, clusters, k1, k2):
        new_cluster = []
        for g in clusters[k1]:
            new_cluster.append(g)

        for g in clusters[k2]:
            if not (g in new_cluster):
                new_cluster.append(g)

        return new_cluster
        
    
    def merge_all_clusters(self, threshold):
        # merge clusters in self.pegase_clusters
        clusters = dict(self.pegase_clusters)
        while True:
            k1, k2, overlap = self.fetch_closest_clusters(clusters)
            #print(k1, k2, overlap)
            if overlap < threshold or k1 == None or k2 == None :
                break
            new_cluster = self.merge_clusters(clusters, k1, k2)
            clusters[k1] = new_cluster
            del clusters[k2]
        return clusters

    def dump_levelized_circuit(self):
        yosys_ports = self.module['ports']
        yosys_cells = self.module['cells']
        levelized_gates = self.levelize()
        g = graphviz.Digraph(name='Levelized netlist (final)',
                             filename='%s_%s_levelized.gv' % (self.basename, self.modulename),
                             format='png')
        g.attr('node', shape='doublecircle')

        edge_from = self.get_incoming_edges()

        with g.subgraph(name='cluster_inputs') as c:
            c.attr(style='filled')
            c.attr(color='lightgrey')
            c.attr(rank='same')
            c.node_attr.update(style='filled', color='white')
            for p in yosys_ports:
                if yosys_ports[p]['direction'] == 'input':
                    for i, b in enumerate(yosys_ports[p]['bits']):
                        c.node('%s_%d' % (p, i))

        g.attr('node', shape='circle')

        for i, gl in enumerate(levelized_gates):
            with g.subgraph(name='cluster_%s' % i) as c:
                c.attr(style='filled')
                c.attr(color='lightgrey')
                c.attr(rank='same')
                c.node_attr.update(style='filled', color='white')
                for n in gl:
                    c.node(n['id'], label=n['type'])

        for i, gl in enumerate(levelized_gates):
            with g.subgraph(name='cluster_%s' % i) as c:
                for n in gl:
                    if n['pin1'] is not None:
                        assert self.net_hash(n['pin1']) in edge_from, 'Unknown input on gate %r' % n['id']
                        gate1 = edge_from[self.net_hash(n['pin1'])]
                        c.edge(gate1['id'], n['id'])
                    if n['pin2'] is not None:
                        assert self.net_hash(n['pin2']) in edge_from, 'Unknown input on gate %r' % n['id']
                        gate2 = edge_from[self.net_hash(n['pin2'])]
                        c.edge(gate2['id'], n['id'])

        g.attr('node', shape='doublecircle')

        with g.subgraph(name='cluster_outputs') as c:
            c.attr(style='filled')
            c.attr(color='lightgrey')
            c.attr(rank='same')
            c.node_attr.update(style='filled', color='white')
            for p in yosys_ports:
                if yosys_ports[p]['direction'] == 'output':
                    bits = yosys_ports[p]['bits']
                    for i, b in enumerate(bits):
                        identifier = '%s_%d' % (p, i)
                        c.node(identifier)
                        net = self.yosys_bits[b]
                        gate = edge_from[self.net_hash(net)]
                        c.edge(gate['id'], identifier)

        g.render(view=False, cleanup=True, directory='_out')

    def dump_clusterized_circuit(self):
        yosys_ports = self.module['ports']
        yosys_cells = self.module['cells']
        g = graphviz.Digraph(name='Clusterized netlist (final)',
                             filename='%s_%s_clusterized.gv' % (self.basename, self.modulename),
                             format='png')
        g.attr('node', shape='doublecircle')

        edge_from = self.get_incoming_edges()

        with g.subgraph(name='cluster_inputs') as c:
            c.attr(style='filled')
            c.attr(color='lightgrey')
            c.attr(rank='same')
            c.node_attr.update(style='filled', color='white')
            for p in yosys_ports:
                if yosys_ports[p]['direction'] == 'input':
                    for i, b in enumerate(yosys_ports[p]['bits']):
                        c.node('%s_%d' % (p, i))

        g.attr('node', shape='circle')

        for key in self.pegase_clusters:
            with g.subgraph(name='cluster_%s' % str(key)) as c:
                c.attr(style='filled')
                c.attr(color='lightgrey')
                c.attr(rank='same')
                c.node_attr.update(style='filled', color='white')
                cluster = self.pegase_clusters[key]
                for n in cluster:
                    if n['type'] != 'PI':
                        c.node('%s_%s' % (n['id'], str(key)), label=n['type'])

        for key in self.pegase_clusters:
            with g.subgraph(name='cluster_%s' % str(key)) as c:
                cluster = self.pegase_clusters[key]
                for n in cluster:
                    if n['pin1'] is not None:
                        assert self.net_hash(n['pin1']) in edge_from, 'Unknown input on gate %r' % n['id']
                        gate1 = edge_from[self.net_hash(n['pin1'])]
                        if gate1['type'] == 'PI':
                            c.edge(gate1['id'], '%s_%s' % (n['id'], str(key)))
                        else:
                            c.edge('%s_%s' % (gate1['id'], str(key)), '%s_%s' % (n['id'], str(key)))
                    if n['pin2'] is not None:
                        assert self.net_hash(n['pin2']) in edge_from, 'Unknown input on gate %r' % n['id']
                        gate2 = edge_from[self.net_hash(n['pin2'])]
                        if gate2['type'] == 'PI':
                            c.edge(gate2['id'], '%s_%s' % (n['id'], str(key)))
                        else:
                            c.edge('%s_%s' % (gate2['id'], str(key)), '%s_%s' % (n['id'], str(key)))

        g.attr('node', shape='doublecircle')

        with g.subgraph(name='cluster_outputs') as c:
            c.attr(style='filled')
            c.attr(color='lightgrey')
            c.attr(rank='same')
            c.node_attr.update(style='filled', color='white')
            for p in yosys_ports:
                if yosys_ports[p]['direction'] == 'output':
                    bits = yosys_ports[p]['bits']
                    for i, b in enumerate(bits):
                        identifier = '%s_%d' % (p, i)
                        c.node(identifier)
                        net = self.yosys_bits[b]
                        gate = edge_from[self.net_hash(net)]
                        for key in self.pegase_clusters:
                            cluster = self.pegase_clusters[key]
                            if gate in cluster:
                                c.edge('%s_%s' % (gate['id'], str(key)), identifier)

        g.render(view=False, cleanup=True, directory='_out')

    def dump_clusterized_and_levelized_circuit(self, levelized_clusters):
        id = 'balanced_' if levelized_clusters is self.balanced_levelized_clusters else ''
        yosys_ports = self.module['ports']
        yosys_cells = self.module['cells']
        g = graphviz.Digraph(name='Clusterized and levelized netlist (final)',
                             filename='%s_%s_%sclusterized_levelized.gv' %
                             (self.basename, self.modulename, id),
                             format='png')
        g.attr('node', shape='doublecircle')

        edge_from = self.get_incoming_edges()

        with g.subgraph(name='cluster_inputs') as c:
            c.attr(style='filled')
            c.attr(color='grey')
            c.attr(rank='same')
            c.node_attr.update(style='filled', color='white')
            for p in yosys_ports:
                if yosys_ports[p]['direction'] == 'input':
                    for i, b in enumerate(yosys_ports[p]['bits']):
                        c.node('%s_%d' % (p, i))

        g.attr('node', shape='circle')

        for i, key in enumerate(self.pegase_clusters):
            with g.subgraph(name='cluster_%s' % str(key)) as c:
                c.attr(style='filled')
                c.attr(color='lightgrey')
                cluster = levelized_clusters[i]
                for j, level in enumerate(cluster):
                    with c.subgraph(name='cluster_%s_%s' % (str(key), j)) as cc:
                        cc.attr(style='filled')
                        cc.attr(color='grey')
                        cc.attr(rank='same')
                        cc.node_attr.update(style='filled', color='white')
                        #gates = self.pegase_clusters[key]
                        for n in level:
                            if n['type'] != 'PI':
                                cc.node('%s_%s' % (n['id'], str(key)), label=n['type'])

        for i, key in enumerate(self.pegase_clusters):
            with g.subgraph(name='cluster_%s' % str(key)) as c:
                cluster = levelized_clusters[i]
                for j, level in enumerate(cluster):
                    with c.subgraph(name='cluster_%s_%s' % (str(key), j)) as cc:
#                        cluster = self.pegase_clusters[key]
                        for n in level:
                            if n['pin1'] is not None:
                                assert self.net_hash(n['pin1']) in edge_from, 'Unknown input on gate %r' % n['id']
                                gate1 = edge_from[self.net_hash(n['pin1'])]
                                if gate1['type'] == 'PI':
                                    cc.edge(gate1['id'], '%s_%s' % (n['id'], str(key)))
                                else:
                                    cc.edge('%s_%s' % (gate1['id'], str(key)), '%s_%s' % (n['id'], str(key)))
                            if n['pin2'] is not None:
                                assert self.net_hash(n['pin2']) in edge_from, 'Unknown input on gate %r' % n['id']
                                gate2 = edge_from[self.net_hash(n['pin2'])]
                                if gate2['type'] == 'PI':
                                    cc.edge(gate2['id'], '%s_%s' % (n['id'], str(key)))
                                else:
                                    cc.edge('%s_%s' % (gate2['id'], str(key)), '%s_%s' % (n['id'], str(key)))

        g.attr('node', shape='doublecircle')

        with g.subgraph(name='cluster_outputs') as c:
            c.attr(style='filled')
            c.attr(color='grey')
            c.attr(rank='same')
            c.node_attr.update(style='filled', color='white')
            for p in yosys_ports:
                if yosys_ports[p]['direction'] == 'output':
                    bits = yosys_ports[p]['bits']
                    for i, b in enumerate(bits):
                        identifier = '%s_%d' % (p, i)
                        c.node(identifier)
                        net = self.yosys_bits[b]
                        gate = edge_from[self.net_hash(net)]
                        for key in self.pegase_clusters:
                            cluster = self.pegase_clusters[key]
                            if gate in cluster:
                                c.edge('%s_%s' % (gate['id'], str(key)), identifier)


        g.render(view=False, cleanup=True, directory='_out')


    def dump_sorted_circuit(self):
        yosys_ports = self.module['ports']
        yosys_cells = self.module['cells']
        sorted_gates = self.topological_sort()
        g = graphviz.Digraph(name='Topologicaly sorted netlist',
                             filename='%s_%s_sorted.gv' % (self.basename, self.modulename),
                             format='png')
        s = graphviz.Digraph(name='Sub')
        s.graph_attr.update(rank='same')
        s.attr('node', shape='doublecircle')

        edge_from = self.get_incoming_edges()

        for p in yosys_ports:
            if yosys_ports[p]['direction'] == 'input':
                for i, b in enumerate(yosys_ports[p]['bits']):
                    identifier = '%s_%d' % (p, i)
                    s.node(identifier)

        for n in sorted_gates:
            if n['type'] == 'PO':
                s.attr('node', shape='doublecircle')
                s.node(n['id'], label=n['type'] if n['type'] is not 'PO' else n['id'])
            else:
                s.attr('node', shape='circle')
                s.node(n['id'], label=n['type'] if n['type'] is not 'PO' else n['id'])

        for n in sorted_gates:
            if n['pin1'] is not None:
                assert self.net_hash(n['pin1']) in edge_from, 'Unknown input on gate %r' % n['id']
                gate1 = edge_from[self.net_hash(n['pin1'])]
                s.edge(gate1['id'], n['id'])
            if n['pin2'] is not None:
                assert self.net_hash(n['pin2']) in edge_from, 'Unknown input on gate %r' % n['id']
                gate2 = edge_from[self.net_hash(n['pin2'])]
                s.edge(gate2['id'], n['id'])

        g.subgraph(s)
        g.render(view=False, cleanup=True, directory='_out')


    def dump_raw_circuit(self):
        yosys_ports = self.module['ports']
        yosys_cells = self.module['cells']
        g = graphviz.Digraph(name='Raw netlist', 
                             filename='%s_%s_raw.gv' % (self.basename, self.modulename),
                             format='png')
        g.attr('node', shape='doublecircle')

        edge_from = self.get_incoming_edges()

        for p in yosys_ports:
            if yosys_ports[p]['direction'] == 'input':
                for i, b in enumerate(yosys_ports[p]['bits']):
                    g.node('%s_%d' % (p, i))

        g.attr('node', shape='circle')

        for n in self.pegase_gates:
            g.node(n['id'], label=n['type'])

        for n in self.pegase_gates:
            if n['pin1'] is not None:
                assert self.net_hash(n['pin1']) in edge_from, 'Unknown input on gate %r' % n['id']
                gate1 = edge_from[self.net_hash(n['pin1'])]
                g.edge(gate1['id'], n['id'])
            if n['pin2'] is not None:
                assert self.net_hash(n['pin2']) in edge_from, 'Unknown input on gate %r' % n['id']
                gate2 = edge_from[self.net_hash(n['pin2'])]
                g.edge(gate2['id'], n['id'])

        g.attr('node', shape='doublecircle')

        for p in yosys_ports:
            if yosys_ports[p]['direction'] == 'output':
                bits = yosys_ports[p]['bits']
                for i, b in enumerate(bits):
                    identifier = '%s_%d' % (p, i)
                    g.node(identifier)
                    net = self.yosys_bits[b]
                    gate = edge_from[self.net_hash(net)]
                    g.edge(gate['id'], identifier)

        g.render(view=False, cleanup=True, directory='_out')


    def __call__(self):
        self.generate_ports()
        self.generate_wires()
        self.generate_cells()
        levelized_gates = self.levelize()
        self.pegase['gates'] = levelized_gates
        self.clusterize()
        self.pegase_clusters = self.merge_all_clusters(30)
        self.levelized_clusters = self.levelize_clusters()        
        self.pegase['gates'] = self.levelized_clusters
        self.balanced_levelized_clusters = self.balance_clusters(self.levelized_clusters)
        self.pegase['gates'] = self.balanced_levelized_clusters

    def dump(self):
        print(json.dumps(self.pegase, separators=(',', ':'), indent=4))


def main():
    args = argparse.ArgumentParser(
        description='Transform yosys json file into levelized/clustered pegase json netlist')
    args.add_argument("-r", "--dump-raw", action="store_true", dest="dump_raw")
    args.add_argument("-s", "--dump-sorted", action="store_true", dest="dump_sorted")
    args.add_argument("-l", "--dump-levelized", action="store_true", dest="dump_levelized")
    args.add_argument("-c", "--dump-clusterized", action="store_true", dest="dump_clusterized")
    args.add_argument("-f", "--dump-clusterized-levelized", action="store_true", dest="dump_clusterized_levelized")
    args.add_argument("-b", "--dump-balanced-clusterized-levelized", action="store_true", dest="dump_balanced_clusterized_levelized")    
    args.add_argument("-v", "--verbose", action="store_true", dest="verbose")
    args.add_argument("-i", "--input", metavar="INPUT_FILE", type=str)
    args.add_argument("output", metavar="OUTPUT_FILE")
    args.add_argument("module", metavar="MODULE", help="module name")
    processed_args = args.parse_args()
    parser = YosysJsonParser(processed_args)
    parser()
    if processed_args.dump_raw:
        parser.dump_raw_circuit()
    if processed_args.dump_sorted:
        parser.dump_sorted_circuit()
    if processed_args.dump_levelized:
        parser.dump_levelized_circuit()
    if processed_args.dump_clusterized:
        parser.dump_clusterized_circuit()
    if processed_args.dump_clusterized_levelized:
        parser.dump_clusterized_and_levelized_circuit(parser.levelized_clusters)
    if processed_args.dump_balanced_clusterized_levelized:
        parser.dump_clusterized_and_levelized_circuit(parser.balanced_levelized_clusters)
    parser.dump()

if __name__ == '__main__':
    main()
