//------------------------------------------------------------------------------
//
// kernel:  simulate
//
// Purpose: 
//	Simulate either a combinatory circuit or a step of a sequential circuit.
// input:
//	* short *width: width of the different levels
//	* gate_t *gates: description of the different gates
//	* char *{input,wire}_pins: actual pin data of the circuit
//	* pin_t *{input,output}_ids: pinout description of the circuit
//	* int N: number of levels
//
// output:
// 	* char *output_pins: actual output pin data of the circuit
//

#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable

typedef enum {
	WIRE = 0,
	INPUT = 1,
	OUTPUT = 2,
} signal_type_t;

typedef enum {
	AND_GATE = 0,
	OR_GATE  = 1,
	XOR_GATE,
	NAND_GATE,
	NOR_GATE,
	NOT_GATE
} gate_type_t;

typedef struct {
	int type;
	int val;	
} __attribute__((packed)) pin_t;

typedef struct {
	gate_type_t type;
	int inputs;
	int outputs;
} __attribute__((packed)) gate_t;

/* bit getter/setter utils */

static inline
char get_idx_local(__local char *v, unsigned idx)
{
	return v[idx / 8] >> (idx % 8) & 1;
}

static inline
char get_idx_global(__constant char *v, unsigned idx)
{
	return v[idx / 8] >> (idx % 8) & 1;
}

static inline
void set_idx_local(__local char *v, unsigned idx, char val)
{
	if (val)
		atomic_or((__local int *)v + (idx / 32),
			(1 << (idx % 32)));
	else
		atomic_and((__local int *)v + (idx / 32),
			~(1 << (idx % 32)));
}

static inline
void set_idx_global(__global char *v, unsigned idx, char val)
{
	if (val)
		atomic_or((__global int *)v + (idx / 32),
			(1 << (idx % 32)));
	else
		atomic_and((__global int *)v + (idx / 32),
			~(1 << (idx % 32)));
}

/* pin fetcher/writer utils */

static inline
char fetch_input(unsigned *counter,
		__constant pin_t *input_ids,
		__constant char *input_pins,
		__local int *wire_pins, int gate_inputs)
{
	pin_t pin = input_ids[gate_inputs + *counter];
	*counter += 1;
	if (pin.type == INPUT)
		return get_idx_global(input_pins, pin.val);
	else /* WIRE */
		return get_idx_local(wire_pins, pin.val);
}

static inline
void write_output(unsigned *counter,
		__constant pin_t *output_ids,
		__global char *output_pins,
		__local int *wire_pins,
		int gate_outputs, char value)
{
	pin_t pin = output_ids[gate_outputs + *counter];
	*counter += 1;
	if (pin.type == OUTPUT)
		set_idx_global(output_pins, pin.val, value);
	else /* WIRE */
		set_idx_local(wire_pins, pin.val, value);
}

/* simulate ONE gate */

void simulate1(gate_t *gate,
		__constant char *input_pins,
		__global char *output_pins,
		__local int *wire_pins,
		__constant pin_t *input_ids,
		__constant pin_t *output_ids)
{
	// Some simple gates
	char nand[4] = {1, 1, 1, 0};
	char nor[4] = {1, 0, 0, 0};
	char and[4] = {0, 0, 0, 1};
	char or[4] = {0, 1, 1, 1};
	char xor[4] = {0, 1, 1, 0};
	char not[2] = {1, 0};
	char *gates_truth[6] = {and, or, xor, nand, nor, not};
	if (gate->type >= AND_GATE && gate->type <= NOR_GATE) {
		/* two input, one output gate */
		/* prologue: fetch inputs */
		unsigned pin_counter = 0;
		char pin1_sig = fetch_input(&pin_counter,
						input_ids,
						input_pins, wire_pins,
						gate->inputs);
		char pin2_sig = fetch_input(&pin_counter,
						input_ids,
						input_pins, wire_pins,
						gate->inputs);

		/* actual computation */
		char *gate_truth = gates_truth[gate->type];
		char output_value = gate_truth[(pin1_sig << 1) + pin2_sig];

		/* epilogue: write answers */
		pin_counter = 0;
		write_output(&pin_counter, output_ids,
				output_pins, wire_pins,
				gate->outputs, output_value);
	} else if (gate->type == NOT_GATE) {
		/* one input, one output gate */
		/* prologue: fetch inputs */
		unsigned pin_counter = 0;
		char pin1_sig = fetch_input(&pin_counter,
						input_ids,
						input_pins, wire_pins,
						gate->inputs);
		/* actual computation */
		char *gate_truth = gates_truth[gate->type];
		char output_value = gate_truth[pin1_sig];
		/* epilogue: write answers */
		pin_counter = 0;
		write_output(&pin_counter, output_ids,
				output_pins, wire_pins,
				gate->outputs, output_value);
	} else {
		/* unknown gate */
	}
}

/* main function (kernel), simulate a whole circuit */

__attribute__ ((work_group_size_hint(1024, 1, 1)))
__kernel void simulate(__global short *width,
	 __constant gate_t *gates,
	 __constant char *input_pins,
	 __global char *output_pins,
	 __local int *wire_pins,
	 __constant pin_t *input_ids,
	 __constant pin_t *output_ids,  int N)
{

	size_t local_id = get_local_id(0);
	unsigned gate_addr = 0;
	for (int level = 0; level < N; level++)
	{
		barrier(CLK_LOCAL_MEM_FENCE);
		unsigned short w = width[level];
		if (local_id < w)
		{
#if 0
			printf("wires: %x, %x %x\n", wire_pins[2], wire_pins[1], wire_pins[0]);
#endif
			gate_t gate = gates[gate_addr + local_id];

			/* simulate one gate/cell */
			simulate1(&gate,
				input_pins, output_pins, wire_pins,
				input_ids, output_ids);

#if 0
			printf("(%x, %x): gate: %x,  gate.o: %x, gate.o.val: %x, pin1: %x, pin2: %x, pino: %x\n",
			    level, local_id, gate.type, gate.o.type, gate.o.val, pin1_sig, pin2_sig, local_output);
#endif

		}
		gate_addr += w;
	}
}
